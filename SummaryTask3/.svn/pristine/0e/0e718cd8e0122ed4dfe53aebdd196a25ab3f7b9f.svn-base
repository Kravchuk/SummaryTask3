package ua.nure.kravchuk.SummaryTask3.util;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import ua.nure.kravchuk.SummaryTask3.constants.Constants;
import ua.nure.kravchuk.SummaryTask3.controller.DOMController;
import ua.nure.kravchuk.SummaryTask3.entity.TouristVoucher;
import ua.nure.kravchuk.SummaryTask3.entity.TouristVouchers;

/**
 * Contains static methods for sorting.
 * 
 * @author P.Kravchuk
 * 
 */
public class Sort {

	// //////////////////////////////////////////////////////////
	// these are comparators
	// //////////////////////////////////////////////////////////

	/**
	 * Sorts vouchers by type.
	 */
	public static final Comparator<TouristVoucher> SORT_VOUCHERS_BY_TYPE = new Comparator<TouristVoucher>() {

		@Override
		public int compare(TouristVoucher o1, TouristVoucher o2) {
			return o1.getType().compareTo(o2.getType());
		}
		
	};

	/**
	 * Sorts vouchers by travel duration.
	 */
	public static final Comparator<TouristVoucher> SORT_VOUCHERS_BY_TRAVEL_DURATION = new Comparator<TouristVoucher>() {

		@Override
		public int compare(TouristVoucher o1, TouristVoucher o2) {
			return o1.getNumberOfDaysNights()-o2.getNumberOfDaysNights();
		}
		
	};

	/**
	 * Sorts vouchers by city.
	 */
	public static final Comparator<TouristVoucher> SORT_VOUCHERS_BY_CITY = new Comparator<TouristVoucher>() {
		@Override
		public int compare(TouristVoucher o1, TouristVoucher o2) {
			return o1.getCity().compareTo(o2.getCity());
		}
	};
	
	
	/**
	 * Sorts vouchers by type.
	 * @param TouristVoucher
	 */
	public static final void sortVouchersByType(TouristVouchers vouchers) {
		Collections.sort(vouchers.getTouristVouchers(), SORT_VOUCHERS_BY_TYPE);
	}

	/**
	 * Sorts vouchers by travel duration.
	 * @param TouristVoucher
	 */
	public static final void sortVouchersByTravelDuration(TouristVouchers vouchers) {
		Collections.sort(vouchers.getTouristVouchers(), SORT_VOUCHERS_BY_TRAVEL_DURATION);
	}

	/**
	 * Sorts vouchers by city.
	 * @param TouristVoucher
	 */
	public static final void sortVouchersByCity(TouristVouchers vouchers) {
		Collections.sort(vouchers.getTouristVouchers(), SORT_VOUCHERS_BY_CITY);
	}

	public static void main(String[] args) throws ParserConfigurationException, 
	SAXException, IOException{
		DOMController domController = new DOMController(Constants.VALID_XML_FILE);
		domController.parse(false);
		TouristVouchers vouchers = domController.getTouristVouchers();

		System.out.println("Standart file");
		System.out.println(vouchers);

		System.out.println("Sort vouchers by type");
		Sort.sortVouchersByType(vouchers);
		System.out.println(vouchers);

		System.out.println("Sort vouchers by travel duration");
		Sort.sortVouchersByTravelDuration(vouchers);
		System.out.println(vouchers);
		
		System.out.println("Sort vouchers by city");
		Sort.sortVouchersByCity(vouchers);
		System.out.println(vouchers);
	}
}
