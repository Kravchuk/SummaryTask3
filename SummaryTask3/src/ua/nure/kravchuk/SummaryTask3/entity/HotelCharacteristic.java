package ua.nure.kravchuk.SummaryTask3.entity;

/**
 * Implements the HotelCharacteristic entity.
 * 
 * @author P.Kravchuk
 * 
 */

public class HotelCharacteristic {
	private int numberOfStars;
	private String food;
	private Room room;
	private Cost cost;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Hotel characteristic").append(System.lineSeparator());
		sb.append("\tNumber of stars :").append(numberOfStars).append(System.lineSeparator());
		sb.append("\tFood :").append(food).append(System.lineSeparator());
		sb.append(room.toString());
		sb.append(cost.toString());
		return sb.toString();
	}

	public int getNumberOfStars() {
		return numberOfStars;
	}

	public void setNumberOfStars(int numberOfStars) {
		this.numberOfStars = numberOfStars;
	}

	public String getFood() {
		return food;
	}

	public void setFood(String food) {
		this.food = food;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Cost getCost() {
		return cost;
	}

	public void setCost(Cost cost) {
		this.cost = cost;
	}

}