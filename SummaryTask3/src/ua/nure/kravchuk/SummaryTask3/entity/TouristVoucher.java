package ua.nure.kravchuk.SummaryTask3.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements the TouristVoucher entity.
 * 
 * @author P.Kravchuk
 * 
 */

public class TouristVoucher {
	private String type;
	private String country;
	private String city;
	private int numberOfDaysNights;
	private String transport;
	private List<HotelCharacteristic> hotelCharacteristicList = new ArrayList<HotelCharacteristic>();

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getNumberOfDaysNights() {
		return numberOfDaysNights;
	}

	public void setNumberOfDaysNights(int numberOfDaysNights) {
		this.numberOfDaysNights = numberOfDaysNights;
	}

	public String getTransport() {
		return transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public List<HotelCharacteristic> getHotelCharacteristicList() {
		return hotelCharacteristicList;
	}

	public void setHotelCharacteristicList(List<HotelCharacteristic> hotelCharacteristicList) {
		this.hotelCharacteristicList = hotelCharacteristicList;
	}

	public HotelCharacteristic getHotelCharacteristic(int i) {
		return hotelCharacteristicList.get(i);
	}

	public void addHotelCharacteristic(HotelCharacteristic hc) {
		hotelCharacteristicList.add(hc);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("TouristVoucher").append(System.lineSeparator());
		sb.append("\tType of voucher is :").append(type).append(System.lineSeparator());
		sb.append("\tCounrty is :").append(country).append(System.lineSeparator());
		sb.append("\tCity is :").append(city).append(System.lineSeparator());
		sb.append("\tNumber of days and nights to stay there :").append(numberOfDaysNights)
				.append(System.lineSeparator());
		sb.append("\tTransport :").append(transport).append(System.lineSeparator());
		for (HotelCharacteristic hotelCharacteristic : hotelCharacteristicList) {
			sb.append(hotelCharacteristic.toString());
		}
		return sb.toString();
	}

}