package ua.nure.kravchuk.SummaryTask3.entity;

/**
 * Implements the Cost entity.
 * 
 * @author P.Kravchuk
 * 
 */

public class Cost {
	private int totalValue;
	private String currency;
	private String services;

	public int getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(int totalValue) {
		this.totalValue = totalValue;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Cost").append(System.lineSeparator());
		sb.append("\tTotal value is :").append(totalValue).append(" ").append(currency).append(System.lineSeparator());
		if (services.length() > 0) {
			sb.append("\tServices :").append(services).append(System.lineSeparator());
		} else {
			sb.append("\tThere is no aditional services.").append(System.lineSeparator());
		}
		return sb.toString();
	}

}
