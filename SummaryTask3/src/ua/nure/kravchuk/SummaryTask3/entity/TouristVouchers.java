package ua.nure.kravchuk.SummaryTask3.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements the TouristVouchers entity.
 * 
 * @author P.Kravchuk
 * 
 */

public class TouristVouchers{
	private List<TouristVoucher> touristVouchers;

	@Override
	public String toString() {
		if (touristVouchers == null || touristVouchers.size() == 0) {
			return "Contains no tourist vouchers";
		}
		StringBuilder result = new StringBuilder();
		int count = 0;
		for (TouristVoucher voucher : touristVouchers) {
			result.append((++count) + " tourist voucher").append(System.lineSeparator());
			result.append(voucher).append(System.lineSeparator());
		}
		return result.toString();

	}

	/**
	 * Returns list with vouchers.
	 * 
	 * @return touristVouchers
	 * 
	 */
	public List<TouristVoucher> getTouristVouchers() {
		if (touristVouchers == null) {
			touristVouchers = new ArrayList<TouristVoucher>();
		}
		return touristVouchers;
	}
}