package ua.nure.kravchuk.SummaryTask3.entity;

/**
 * Implements the Room entity.
 * 
 * @author P.Kravchuk
 * 
 */

public class Room {
	private int places;
	private boolean television;
	private boolean fridge;
	private boolean conditioner;
	private boolean bathroom;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Room").append(System.lineSeparator());
		sb.append("\tNumber of sleeping places :").append(places).append(System.lineSeparator());
		sb.append("\tTelevision ");
		if (television) {
			sb.append("+").append(System.lineSeparator());
		} else {
			sb.append("-").append(System.lineSeparator());
		}
		sb.append("\tConditioner ");
		if (conditioner) {
			sb.append("+").append(System.lineSeparator());
		} else {
			sb.append("-").append(System.lineSeparator());
		}
		sb.append("\tFridge ");
		if (fridge) {
			sb.append("+").append(System.lineSeparator());
		} else {
			sb.append("-").append(System.lineSeparator());
		}
		sb.append("\tBathroom ");
		if (bathroom) {
			sb.append("+").append(System.lineSeparator());
		} else {
			sb.append("-").append(System.lineSeparator());
		}
		return sb.toString();
	}

	public int getPlaces() {
		return places;
	}

	public void setPlaces(int places) {
		this.places = places;
	}

	public boolean isTelevision() {
		return television;
	}

	public void setTelevision(boolean television) {
		this.television = television;
	}

	public boolean isConditioner() {
		return conditioner;
	}

	public void setConditioner(boolean conditioner) {
		this.conditioner = conditioner;
	}

	public boolean isFridge() {
		return fridge;
	}

	public void setFridge(boolean fridge) {
		this.fridge = fridge;
	}

	public boolean isBathroom() {
		return bathroom;
	}

	public void setBathroom(boolean bathroom) {
		this.bathroom = bathroom;
	}

}