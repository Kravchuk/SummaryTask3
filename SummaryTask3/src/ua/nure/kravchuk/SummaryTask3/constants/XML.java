package ua.nure.kravchuk.SummaryTask3.constants;

/**
 * Holds entities declared in XSD document.
 * 
 * @author P.Kravchuk
 *
 */
public enum XML {
	TOURIST_VOUCHERS("TouristVouchers"), TOURIST_VOUCHER("TouristVoucher"), TYPE("Type"), COUNTRY("Country"), CITY(
			"City"), NUMBER_OF_DAYS_NIGHTS("NumberOfDays-Nights"), TRANSPORT("Transport"), HOTEL_CHARACTERISTIC(
					"HotelCharacteristic"), NUMBER_OF_STARS("NumberOfStars"), FOOD("Food"), ROOM("Room"), PLACES(
							"Places"), TELEVISION("Television"), CONDITIONER("—onditioner"), FRIDGE("Fridge"), BATHROOM(
									"Bathroom"), COST("Cost"), TOTAL_VALUE(
											"TotalValue"), CURRENCY("Currency"), SERVICES("Services");

	private String value;

	XML(String value) {
		this.value = value;
	}

	/**
	 * Determines if a name is equal to the string value wrapped by this enum
	 * element.<br/>
	 * If a SAX/StAX parser make all names of elements and attributes interned
	 * you can use
	 * 
	 * <pre>
	 * return value == name;
	 * </pre>
	 * 
	 * instead
	 * 
	 * <pre>
	 * return value.equals(name);
	 * </pre>
	 * 
	 * 
	 * @param name
	 *            string to compare with value.
	 * @return value.equals(name)
	 */
	public boolean equalsTo(String name) {
		return value.equals(name);
	}

	
	/**
	 * Returns String value of XML.
	 * 
	 * @return value
	 *
	 */
	public String value() {
		return value;
	}
}
