package ua.nure.kravchuk.SummaryTask3.constants;

/**
 * Holds application constants.
 * 
 * @author P.Kravchuk
 * 
 */
public final class Constants {
	
	public static final String VALID_XML_FILE = "input.xml";
	public static final String INVALID_XML_FILE = "input-invalid";
	public static final String XSD_FILE = "input.xsd";

	public static final String FEATURE_TURN_VALIDATION_ON = "http://xml.org/sax/features/validation";
	public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON = "http://apache.org/xml/features/validation/schema";

}
