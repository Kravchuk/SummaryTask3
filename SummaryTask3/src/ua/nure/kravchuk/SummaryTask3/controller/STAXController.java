package ua.nure.kravchuk.SummaryTask3.controller;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


import ua.nure.kravchuk.SummaryTask3.constants.Constants;
import ua.nure.kravchuk.SummaryTask3.constants.XML;
import ua.nure.kravchuk.SummaryTask3.entity.Cost;
import ua.nure.kravchuk.SummaryTask3.entity.HotelCharacteristic;
import ua.nure.kravchuk.SummaryTask3.entity.Room;
import ua.nure.kravchuk.SummaryTask3.entity.TouristVoucher;
import ua.nure.kravchuk.SummaryTask3.entity.TouristVouchers;

/**
 * Controller for StAX parser.
 * 
 * @author P.Kravchuk
 * 
 */
public class STAXController extends DefaultHandler {
	
	/**name of XML */
	private String xmlFileName;

	/** main container */
	private TouristVouchers touristVouchers;

	/** getter for vouchers */
	public TouristVouchers geTouristVouchers() {
		return touristVouchers;
	}
	/**STAX constructor where we need specify the name*/
	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	
	/**
	 * Parses XML document with StAX (based on event reader). There is no validation during the
	 * parsing.
	 */
	public void parse() throws ParserConfigurationException, SAXException,
			IOException, XMLStreamException {

		TouristVoucher touristVoucher = null;
		HotelCharacteristic hCharacteristic = null;
		Room room = null;
		Cost cost = null;
		
		// current element name holder
		String currentElement = null;
		
		XMLInputFactory factory = XMLInputFactory.newInstance();
		
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(
				new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			// skip any empty content
			if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
				continue;
			}

			// handler for start tags
			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();

				if (XML.TOURIST_VOUCHERS.equalsTo(currentElement)) {
					touristVouchers = new TouristVouchers();
					continue;
				}

				if (XML.TOURIST_VOUCHER.equalsTo(currentElement)) {
					touristVoucher = new TouristVoucher();
					continue;
				}
				
				if (XML.HOTEL_CHARACTERISTIC.equalsTo(currentElement)) {
					hCharacteristic = new HotelCharacteristic();
					touristVoucher.addHotelCharacteristic(hCharacteristic);
					continue;
				}
				
				if (XML.ROOM.equalsTo(currentElement)) {
					room = new Room();
					hCharacteristic.setRoom(room);
					continue;
				}
				
				if (XML.COST.equalsTo(currentElement)) {
					cost = new Cost();
					hCharacteristic.setCost(cost);
					continue;
				}
			}

			// handler for contents
			if (event.isCharacters()) {
				Characters characters = event.asCharacters();

				if (XML.TYPE.equalsTo(currentElement)) {
					touristVoucher.setType(characters.getData());
					continue;
				}

				if (XML.COUNTRY.equalsTo(currentElement)) {
					touristVoucher.setCountry(characters.getData());
					continue;
				}

				if (XML.CITY.equalsTo(currentElement)) {
					touristVoucher.setCity(characters.getData());
					continue;
				}

				if (XML.NUMBER_OF_DAYS_NIGHTS.equalsTo(currentElement)) {
					int tmp = Integer.parseInt(characters.getData());
					touristVoucher.setNumberOfDaysNights(tmp);
					continue;
				}

				if (XML.TRANSPORT.equalsTo(currentElement)) {
					touristVoucher.setTransport(characters.getData());
					continue;
				}


				if (XML.NUMBER_OF_STARS.equalsTo(currentElement)) {
					int tmp = Integer.parseInt(characters.getData());
					hCharacteristic.setNumberOfStars(tmp);
					continue;
				}

				if (XML.FOOD.equalsTo(currentElement)) {
					hCharacteristic.setFood(characters.getData());
					continue;
				}
				
				if (XML.PLACES.equalsTo(currentElement)) {
					int tmp = Integer.parseInt(characters.getData());
					room.setPlaces(tmp);
					continue;
				}
				
				if (XML.TELEVISION.equalsTo(currentElement)) {
					boolean tmp = Boolean.valueOf(characters.getData());
					room.setTelevision(tmp);
					continue;
				}
				
				if (XML.FRIDGE.equalsTo(currentElement)) {
					boolean tmp = Boolean.valueOf(characters.getData());
					room.setFridge(tmp);
					continue;
				}

				if (XML.CONDITIONER.equalsTo(currentElement)) {
					boolean tmp = Boolean.valueOf(characters.getData());
					room.setConditioner(tmp);
					continue;
				}
				
				if (XML.BATHROOM.equalsTo(currentElement)) {
					boolean tmp = Boolean.valueOf(characters.getData());
					room.setBathroom(tmp);
					continue;
				}
				
				if (XML.TOTAL_VALUE.equalsTo(currentElement)) {
					int tmp = Integer.parseInt(characters.getData());
					cost.setTotalValue(tmp);
					continue;
				}

				if (XML.CURRENCY.equalsTo(currentElement)) {
					cost.setCurrency(characters.getData());
					continue;
				}

				if (XML.SERVICES.equalsTo(currentElement)) {
					cost.setServices(characters.getData());
					continue;
				}
				
			}

			// handler for end tags
			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();


				if (XML.TOURIST_VOUCHER.equalsTo(localName)) {
					touristVouchers.getTouristVouchers().add(touristVoucher);
				}
			}
		}
		reader.close();
	}
	
	public static void main(String[] args) throws ParserConfigurationException, 
	SAXException, IOException, XMLStreamException{

		// try to parse (valid) XML file (success)
		STAXController staxContr = new STAXController(Constants.VALID_XML_FILE);
		
		//do parse (success)
		staxContr.parse(); 

		// obtain container
		TouristVouchers tVouchers = staxContr.geTouristVouchers();

		// we have Test object at this point:
		System.out.println("====================================");
		System.out.print("All vouchers: \n" + tVouchers);
		System.out.println("====================================");
	}
}
