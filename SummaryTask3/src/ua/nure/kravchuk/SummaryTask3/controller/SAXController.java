package ua.nure.kravchuk.SummaryTask3.controller;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


import ua.nure.kravchuk.SummaryTask3.entity.Cost;
import ua.nure.kravchuk.SummaryTask3.entity.HotelCharacteristic;
import ua.nure.kravchuk.SummaryTask3.entity.Room;
import ua.nure.kravchuk.SummaryTask3.entity.TouristVoucher;
import ua.nure.kravchuk.SummaryTask3.entity.TouristVouchers;
import ua.nure.kravchuk.SummaryTask3.constants.Constants;
import ua.nure.kravchuk.SummaryTask3.constants.XML;

/**
 * Controller for SAX parser.
 * 
 * @author P.Kravchuk
 * 
 */
public class SAXController extends DefaultHandler {
	/**name of XML */
	private String xmlFileName;

	/** current element name holder */
	private String currentElement;

	/** main container */
	private TouristVouchers touristVouchers;
	
	/** field for vouchers */
	private TouristVoucher touristVoucher;
	
	/** field for hotel characteristic */
	private HotelCharacteristic hotelCharacteristic;
	
	/** field for room */
	private Room room;
	
	/** field for cost */
	private Cost cost;

	/**SAX constructor where we need specify the name*/
	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	/**
	 * Parses XML document.
	 * 
	 * @param validate
	 *            If true validate XML document against its XML schema. With
	 *            this parameter it is possible make parser validating.
	 */

	public void parse(boolean validate) throws ParserConfigurationException, SAXException, IOException {

		// obtain sax parser factory
		SAXParserFactory factory = SAXParserFactory.newInstance();

		// XML document contains namespaces
		factory.setNamespaceAware(true);

		// set validation
		if (validate) {
			factory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			factory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, this);
	}

	
	/** 
	 * Error handler implementation 
	 */
	
	@Override
	public void error(org.xml.sax.SAXParseException e) throws SAXException {
		// if XML document not valid just throw exception
		throw e;
	};
	
	/**
	 * Getter for tourist vouchers.
	 * 
	 * @return touristVouchers
	 */
	public TouristVouchers getTouristVouchers() {
		return touristVouchers;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentElement = localName;

		if (XML.TOURIST_VOUCHERS.equalsTo(currentElement)) {
			touristVouchers = new TouristVouchers();
			return;
		}

		if (XML.TOURIST_VOUCHER.equalsTo(currentElement)) {
			touristVoucher = new TouristVoucher();
		}


		if (XML.HOTEL_CHARACTERISTIC.equalsTo(currentElement)) {
			hotelCharacteristic = new HotelCharacteristic();
			touristVoucher.addHotelCharacteristic(hotelCharacteristic);
			return;
		}	

		if (XML.ROOM.equalsTo(currentElement)) {
			room = new Room();
			hotelCharacteristic.setRoom(room);
			return;
		}
		
		if (XML.COST.equalsTo(currentElement)) {
			cost = new Cost();
			hotelCharacteristic.setCost(cost);
			return;
		}

	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {

		String elementText = new String(ch, start, length).trim();

		// return if content is empty
		if (elementText.isEmpty()) {
			return;
		}

		if (XML.TYPE.equalsTo(currentElement)) {
			touristVoucher.setType(elementText);
			return;
		}

		if (XML.COUNTRY.equalsTo(currentElement)) {
			touristVoucher.setCountry(elementText);
			return;
		}

		if (XML.CITY.equalsTo(currentElement)) {
			touristVoucher.setCity(elementText);
			return;
		}

		if (XML.NUMBER_OF_DAYS_NIGHTS.equalsTo(currentElement)) {
			int tmp = Integer.parseInt(elementText);
			touristVoucher.setNumberOfDaysNights(tmp);
			return;
		}

		if (XML.TRANSPORT.equalsTo(currentElement)) {
			touristVoucher.setTransport(elementText);
			return;
		}


		if (XML.NUMBER_OF_STARS.equalsTo(currentElement)) {
			int tmp = Integer.parseInt(elementText);
			hotelCharacteristic.setNumberOfStars(tmp);
			return;
		}

		if (XML.FOOD.equalsTo(currentElement)) {
			hotelCharacteristic.setFood(elementText);
			return;
		}
		
		if (XML.PLACES.equalsTo(currentElement)) {
			int tmp = Integer.parseInt(elementText);
			room.setPlaces(tmp);
			return;
		}
		
		if (XML.TELEVISION.equalsTo(currentElement)) {
			boolean tmp = Boolean.valueOf(elementText);
			room.setTelevision(tmp);
			return;
		}
		
		if (XML.FRIDGE.equalsTo(currentElement)) {
			boolean tmp = Boolean.valueOf(elementText);
			room.setFridge(tmp);
			return;
		}

		if (XML.CONDITIONER.equalsTo(currentElement)) {
			boolean tmp = Boolean.valueOf(elementText);
			room.setConditioner(tmp);
			return;
		}
		
		if (XML.BATHROOM.equalsTo(currentElement)) {
			boolean tmp = Boolean.valueOf(elementText);
			room.setBathroom(tmp);
			return;
		}
		
		if (XML.TOTAL_VALUE.equalsTo(currentElement)) {
			int tmp = Integer.parseInt(elementText);
			cost.setTotalValue(tmp);
			return;
		}

		if (XML.CURRENCY.equalsTo(currentElement)) {
			cost.setCurrency(elementText);
			return;
		}

		if (XML.SERVICES.equalsTo(currentElement)) {
			cost.setServices(elementText);
			return;
		}
		
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		if (XML.TOURIST_VOUCHER.equalsTo(localName)) {
			// just add voucher to container
			touristVouchers.getTouristVouchers().add(touristVoucher);
			return;
		}
	}

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException{

		// try to parse valid XML file (success)
		SAXController saxContr = new SAXController(Constants.VALID_XML_FILE);

		// do parse with validation on (success)
		saxContr.parse(true);

		// obtain container
		TouristVouchers touristVouchers = saxContr.getTouristVouchers();

		// we have Test object at this point:
		System.out.print("Here is the test: \n" + touristVouchers);

		// now try to parse NOT valid XML (failed)
		saxContr = new SAXController(Constants.INVALID_XML_FILE);
		try {
			// do parse with validation on (failed)
			saxContr.parse(true);
		} catch (Exception ex) {
			System.err.println("Validation is failed:\n" + ex.getMessage());
			System.err.println("Try to print test object:" + saxContr.getTouristVouchers());
		}

		// and now try to parse NOT valid XML with validation off (success)
		saxContr.parse(false);

		// we have tVouchers object at this point:
		System.out.print("All tourist vouchers: \n" + saxContr.getTouristVouchers());
	}
}
