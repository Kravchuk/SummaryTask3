package ua.nure.kravchuk.SummaryTask3.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import ua.nure.kravchuk.SummaryTask3.constants.Constants;
import ua.nure.kravchuk.SummaryTask3.constants.XML;
import ua.nure.kravchuk.SummaryTask3.entity.Cost;
import ua.nure.kravchuk.SummaryTask3.entity.HotelCharacteristic;
import ua.nure.kravchuk.SummaryTask3.entity.Room;
import ua.nure.kravchuk.SummaryTask3.entity.TouristVoucher;
import ua.nure.kravchuk.SummaryTask3.entity.TouristVouchers;

/**
 * Controller for DOM parser.
 * 
 * @author P.kravchuk
 * 
 */
public class DOMController {

	private String xmlFileName;

	// main container
	private TouristVouchers touristVouchers;

	/**
	 * Main constructor for DOMController.
	 * 
	 * @param xmlFileName
	 *            xml file name to read from
	 * 
	 */
	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	/**
	 * Getter for tourist vouchers.
	 * 
	 * @return touristVouchers
	 * 
	 */
	public TouristVouchers getTouristVouchers() {
		return touristVouchers;
	}

	/**
	 * Parses XML document.
	 * 
	 * @param validate
	 *            If true validate XML document against its XML schema.
	 */
	public void parse(boolean validate) throws ParserConfigurationException, SAXException, IOException {

		// obtain DOM parser
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		// set properties for Factory

		// XML document contains namespaces
		dbf.setNamespaceAware(true);

		// make parser validating
		if (validate) {
			// turn validation on
			dbf.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);

			// turn on xsd validation
			dbf.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		DocumentBuilder db = dbf.newDocumentBuilder();

		// set error handler
		db.setErrorHandler(new DefaultHandler() {
			@Override
			public void error(SAXParseException e) throws SAXException {
				// throw exception if XML document is NOT valid
				throw e;
			}
		});

		// parse XML document
		Document document = db.parse(xmlFileName);

		// get root element
		Element root = document.getDocumentElement();

		// create container
		touristVouchers = new TouristVouchers();

		// obtain questions nodes
		NodeList touristVouchersNodes = root.getElementsByTagName(XML.TOURIST_VOUCHER.value());

		// process questions nodes
		for (int j = 0; j < touristVouchersNodes.getLength(); j++) {
			TouristVoucher touristVoucher = getTouristVoucher(touristVouchersNodes.item(j));
			// add touristVoucher to container
			touristVouchers.getTouristVouchers().add(touristVoucher);
		}
	}

	/**
	 * Extracts cost object from the question XML node.
	 * 
	 * @param qNode
	 *            Question node.
	 * @return Question object.
	 */

	private Cost getCost(Node qNode) {
		Cost cost = new Cost();
		Element qElement = (Element) qNode;

		// set total value
		Node qtNode = qElement.getElementsByTagName(XML.TOTAL_VALUE.value()).item(0);
		int tmp = Integer.parseInt(qtNode.getTextContent());
		cost.setTotalValue(tmp);

		// set currency
		qtNode = qElement.getElementsByTagName(XML.CURRENCY.value()).item(0);
		String s = qtNode.getTextContent();
		cost.setCurrency(s);

		// set services
		if (qElement.getElementsByTagName(XML.SERVICES.value()).item(0) != null) {
			qtNode = qElement.getElementsByTagName(XML.SERVICES.value()).item(0);
			s = qtNode.getTextContent();
			cost.setServices(s);
		}
		return cost;
	}

	/**
	 * Extracts hotel characteristic list from the answer XML node.
	 * 
	 * @param aNode
	 *            Answer node.
	 * @return Answer object.
	 */
	private List<HotelCharacteristic> getHotelCharacteristic(NodeList qNodeList) {
		List<HotelCharacteristic> hcl = new ArrayList<HotelCharacteristic>();
		Element qElement = null;
		HotelCharacteristic hc = null;
		Node qtNode = null;
		for (int i = 0; i < qNodeList.getLength(); i++) {
			hc = new HotelCharacteristic();
			qElement = (Element) qNodeList.item(i);

			qtNode = qElement.getElementsByTagName(XML.NUMBER_OF_STARS.value()).item(0);
			int tmp = Integer.parseInt(qtNode.getTextContent());
			hc.setNumberOfStars(tmp);

			qtNode = qElement.getElementsByTagName(XML.FOOD.value()).item(0);
			String s = qtNode.getTextContent();
			hc.setFood(s);

			qtNode = qElement.getElementsByTagName(XML.ROOM.value()).item(0);
			hc.setRoom(getRoom(qtNode));

			qtNode = qElement.getElementsByTagName(XML.COST.value()).item(0);
			hc.setCost(getCost(qtNode));
			hcl.add(hc);
		}

		return hcl;
	}

	/**
	 * Extracts room object from the answer XML node.
	 * 
	 * @param aNode
	 *            Answer node.
	 * @return Answer object.
	 */
	private Room getRoom(Node qNode) {
		Room room = new Room();
		Element qElement = (Element) qNode;

		Node qtNode = qElement.getElementsByTagName(XML.PLACES.value()).item(0);
		int tmp = Integer.parseInt(qtNode.getTextContent());
		room.setPlaces(tmp);

		qtNode = qElement.getElementsByTagName(XML.TELEVISION.value()).item(0);
		boolean b = Boolean.parseBoolean(qtNode.getTextContent());
		room.setTelevision(b);

		qtNode = qElement.getElementsByTagName(XML.FRIDGE.value()).item(0);
		b = Boolean.parseBoolean(qtNode.getTextContent());
		room.setFridge(b);

		qtNode = qElement.getElementsByTagName(XML.CONDITIONER.value()).item(0);
		b = Boolean.parseBoolean(qtNode.getTextContent());
		room.setConditioner(b);

		qtNode = qElement.getElementsByTagName(XML.BATHROOM.value()).item(0);
		b = Boolean.parseBoolean(qtNode.getTextContent());
		room.setBathroom(b);

		return room;
	}

	/**
	 * Extracts touristVoucher object from the answer XML node.
	 * 
	 * @param aNode
	 *            Answer node.
	 * @return Answer object.
	 */
	private TouristVoucher getTouristVoucher(Node qNode) {
		TouristVoucher touristVoucher = new TouristVoucher();
		Element qElement = (Element) qNode;

		Node qtNode = qElement.getElementsByTagName(XML.TYPE.value()).item(0);
		String s = qtNode.getTextContent();
		touristVoucher.setType(s);

		qtNode = qElement.getElementsByTagName(XML.COUNTRY.value()).item(0);
		s = qtNode.getTextContent();
		touristVoucher.setCountry(s);

		qtNode = qElement.getElementsByTagName(XML.CITY.value()).item(0);
		s = qtNode.getTextContent();
		touristVoucher.setCity(s);

		qtNode = qElement.getElementsByTagName(XML.NUMBER_OF_DAYS_NIGHTS.value()).item(0);
		int tmp = Integer.parseInt(qtNode.getTextContent());
		touristVoucher.setNumberOfDaysNights(tmp);

		qtNode = qElement.getElementsByTagName(XML.TRANSPORT.value()).item(0);
		s = qtNode.getTextContent();
		touristVoucher.setTransport(s);

		NodeList qtNodeList = qElement.getElementsByTagName(XML.HOTEL_CHARACTERISTIC.value());
		touristVoucher.setHotelCharacteristicList(getHotelCharacteristic(qtNodeList));

		return touristVoucher;
	}

	/**
	 * Extracts touristVoucher object from the answer XML node.
	 * 
	 * @param touristVouchers
	 *            TouristVouchers list.
	 * @return Answer object.
	 */
	public static Document getDocument(TouristVouchers touristVouchers) throws ParserConfigurationException {

		// obtain DOM parser
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		// set properties for Factory

		// XML document contains namespaces
		dbf.setNamespaceAware(true);

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		// create root element
		Element tElement = document.createElement(XML.TOURIST_VOUCHERS.value());

		// add root element
		document.appendChild(tElement);

		// add touristVouchers elements
		for (TouristVoucher touristVoucher : touristVouchers.getTouristVouchers()) {

			// add touristVoucher
			Element mainVoucher = document.createElement(XML.TOURIST_VOUCHER.value());
			tElement.appendChild(mainVoucher);

			// add type
			Element qtElement = document.createElement(XML.TYPE.value());
			qtElement.setTextContent(touristVoucher.getType());
			mainVoucher.appendChild(qtElement);

			// add country
			qtElement = document.createElement(XML.COUNTRY.value());
			qtElement.setTextContent(touristVoucher.getCountry());
			mainVoucher.appendChild(qtElement);

			// add city
			qtElement = document.createElement(XML.CITY.value());
			qtElement.setTextContent(touristVoucher.getCity());
			mainVoucher.appendChild(qtElement);

			// add number of days/nights
			qtElement = document.createElement(XML.NUMBER_OF_DAYS_NIGHTS.value());
			String tmp = Integer.toString(touristVoucher.getNumberOfDaysNights());
			qtElement.setTextContent(tmp);
			mainVoucher.appendChild(qtElement);

			// add transport
			qtElement = document.createElement(XML.TRANSPORT.value());
			qtElement.setTextContent(touristVoucher.getTransport());
			mainVoucher.appendChild(qtElement);

			Element mainHotelCh = null;

			for (int i = 0; i < touristVoucher.getHotelCharacteristicList().size(); i++) {
				// add hotel characteristic
				mainHotelCh = document.createElement(XML.HOTEL_CHARACTERISTIC.value());
				mainVoucher.appendChild(mainHotelCh);

				// add number of stars
				qtElement = document.createElement(XML.NUMBER_OF_STARS.value());
				tmp = Integer.toString(touristVoucher.getHotelCharacteristic(i).getNumberOfStars());
				qtElement.setTextContent(tmp);
				mainHotelCh.appendChild(qtElement);

				// add food
				qtElement = document.createElement(XML.FOOD.value());
				qtElement.setTextContent(touristVoucher.getHotelCharacteristic(i).getFood());
				mainHotelCh.appendChild(qtElement);

				// add room
				Element mainRoom = document.createElement(XML.ROOM.value());
				mainHotelCh.appendChild(mainRoom);

				// add places
				qtElement = document.createElement(XML.PLACES.value());
				tmp = Integer.toString(touristVoucher.getHotelCharacteristic(i).getRoom().getPlaces());
				qtElement.setTextContent(tmp);
				mainRoom.appendChild(qtElement);

				// add television
				qtElement = document.createElement(XML.TELEVISION.value());
				tmp = Boolean.toString(touristVoucher.getHotelCharacteristic(i).getRoom().isTelevision());
				qtElement.setTextContent(tmp);
				mainRoom.appendChild(qtElement);

				// add fridge
				qtElement = document.createElement(XML.FRIDGE.value());
				tmp = Boolean.toString(touristVoucher.getHotelCharacteristic(i).getRoom().isFridge());
				qtElement.setTextContent(tmp);
				mainRoom.appendChild(qtElement);

				// add conditioner
				qtElement = document.createElement(XML.CONDITIONER.value());
				tmp = Boolean.toString(touristVoucher.getHotelCharacteristic(i).getRoom().isConditioner());
				qtElement.setTextContent(tmp);
				mainRoom.appendChild(qtElement);

				// add bathroom
				qtElement = document.createElement(XML.BATHROOM.value());
				tmp = Boolean.toString(touristVoucher.getHotelCharacteristic(i).getRoom().isBathroom());
				qtElement.setTextContent(tmp);
				mainRoom.appendChild(qtElement);

				// add cost
				Element mainCost = document.createElement(XML.COST.value());
				mainHotelCh.appendChild(mainCost);

				// add total value
				qtElement = document.createElement(XML.TOTAL_VALUE.value());
				tmp = Integer.toString(touristVoucher.getHotelCharacteristic(i).getCost().getTotalValue());
				qtElement.setTextContent(tmp);
				mainCost.appendChild(qtElement);

				// add currency
				qtElement = document.createElement(XML.CURRENCY.value());
				qtElement.setTextContent(touristVoucher.getHotelCharacteristic(i).getCost().getCurrency());
				mainCost.appendChild(qtElement);

				// add services
				qtElement = document.createElement(XML.SERVICES.value());
				qtElement.setTextContent(touristVoucher.getHotelCharacteristic(i).getCost().getServices());
				mainCost.appendChild(qtElement);
			}
		}

		return document;
	}

	/**
	 * Saves Test object to XML file.
	 * 
	 * @param test
	 *            Test object to be saved.
	 * @param xmlFileName
	 *            Output XML file name.
	 */
	public static void saveToXML(Document document, String xmlFileName) throws TransformerException {

		StreamResult result = new StreamResult(new File(xmlFileName));

		// set up transformation
		TransformerFactory tf = TransformerFactory.newInstance();
		javax.xml.transform.Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");

		// run transformation
		t.transform(new DOMSource(document), result);
	}

	/**
	 * Save DOM to XML.
	 * 
	 * @param document
	 *            DOM to be saved.
	 * @param xmlFileName
	 *            Output XML file name.
	 */
	public static void saveToXML(TouristVouchers touristVouchers, String xmlFileName)
			throws ParserConfigurationException, TransformerException {
		// Test -> DOM -> XML
		saveToXML(getDocument(touristVouchers), xmlFileName);
	}

	public static void main(String[] args)
			throws ParserConfigurationException, IOException, SAXException, TransformerException {

		DOMController domContr = new DOMController(Constants.INVALID_XML_FILE);

		try {
			// parse with validation (failed)
			domContr.parse(true);
		} catch (SAXException ex) {
			System.err.println("XML not valid");
			System.err.println("Test object --> " + domContr.getTouristVouchers());
		}

		// try to parse NOT valid XML document with validation off (success)
		domContr.parse(false);

		// we have Test object at this point:
		System.out.print("All touristVouchers: \n" + domContr.getTouristVouchers());

		// save test in XML file
		TouristVouchers touristVouchers = domContr.getTouristVouchers();
		DOMController.saveToXML(touristVouchers, Constants.INVALID_XML_FILE + ".dom-result.xml");

	}

}
