package ua.nure.kravchuk.SummaryTask3;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import ua.nure.kravchuk.SummaryTask3.controller.DOMController;
import ua.nure.kravchuk.SummaryTask3.controller.SAXController;
import ua.nure.kravchuk.SummaryTask3.controller.STAXController;
import ua.nure.kravchuk.SummaryTask3.entity.TouristVouchers;
import ua.nure.kravchuk.SummaryTask3.util.Sort;

/**
 * Entry point for st3 example (simple version).
 * 
 * @author P.Kravchuk
 *
 */

public class Main {
	private static final String SAX_OUTPUT_FILENAME = "output.sax.xml";
	private static final String DOM_OUTPUT_FILENAME = "output.dom.xml";
	private static final String STAX_OUTPUT_FILENAME = "output.stax.xml";

	public static void main(String[] args)
			throws ParserConfigurationException, TransformerException, SAXException, IOException, XMLStreamException {
		if (args.length != 1) {
			System.out.println("Incorrect input!!!");
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		// ////////////////////////////////////////////////////////
		// // DOM
		// ////////////////////////////////////////////////////////
		//
		// // get
		DOMController domController = new DOMController(xmlFileName);
		domController.parse(true);
		TouristVouchers tvs = domController.getTouristVouchers();

		// sort (case 1)
		Sort.sortVouchersByCity(tvs);

		// save
		DOMController.saveToXML(tvs, DOM_OUTPUT_FILENAME);
		System.out.println("Output ==> " + DOM_OUTPUT_FILENAME);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse(true);
		tvs = saxController.getTouristVouchers();

		// sort (case 2)
		Sort.sortVouchersByTravelDuration(tvs);

		// other way:
		DOMController.saveToXML(tvs, SAX_OUTPUT_FILENAME);
		System.out.println("Output ==> " + SAX_OUTPUT_FILENAME);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		tvs = staxController.geTouristVouchers();

		// sort (case 3)
		Sort.sortVouchersByType(tvs);

		// save
		DOMController.saveToXML(tvs, STAX_OUTPUT_FILENAME);
		System.out.println("Output ==> " + STAX_OUTPUT_FILENAME);

	}
}
