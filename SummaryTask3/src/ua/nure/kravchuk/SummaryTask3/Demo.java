package ua.nure.kravchuk.SummaryTask3;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

/**
 * Demo class to run project WO command line.
 * 
 * @author P.Kravchuk
 * 
 */
public class Demo {
	public static void main(String[] args) throws SAXException, 
	ParserConfigurationException, IOException, TransformerException, XMLStreamException{
		Main.main(new String[]{"input.xml"});
	}
}
